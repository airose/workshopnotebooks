# WorkshopNotebooks

Publicly available notebooks for workshops I give.

## PyLadies Hamburg: October 19, 2021

### Build Your Own Audio Processing Package Using Poetry

Notebook: `./poetry_package_acoustics/WorkshopTeaser.ipynb`
